"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    User.init({
        email: DataTypes.STRING,
        password: DataTypes.STRING,
        name: DataTypes.STRING,
        birthdate: DataTypes.DATE,
        photo_profile: DataTypes.STRING,
        photo_profile_id: DataTypes.STRING,
        forgot_password_token: DataTypes.STRING,
        forgot_password_token_exp: DataTypes.DATE,
    }, {
        sequelize,
        modelName: "User",
    });
    return User;
};
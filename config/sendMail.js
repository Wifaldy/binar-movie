const nodemailer = require("nodemailer");
require("dotenv").config();
module.exports = async(from, to, subject, text, html) => {
    const transport = nodemailer.createTransport({
        host: "smtp.mailtrap.io",
        port: 2525,
        secure: false,
        auth: {
            user: process.env.NODEMAILER_USER,
            pass: process.env.NODEMAILER_PASS,
        },
    });

    let info = await transport.sendMail({
        from: from,
        to: to,
        subject: subject,
        text: text,
        html: html,
    });
    return info;
};
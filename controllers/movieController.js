const { Movie } = require("../models");

exports.getMovies = async(req, res, next) => {
    try {
        const movies = await Movie.findAll();
        res.status(200).json({ movies });
    } catch (err) {
        next(err);
    }
};
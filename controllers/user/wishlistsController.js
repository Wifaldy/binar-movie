const { Wishlist, Movie } = require("../../models");

exports.postAddWishlist = async(req, res, next) => {
    try {
        const { id } = req.params;
        const movie = await Movie.findByPk(id);
        if (!movie) {
            throw {
                status: 404,
                message: "Movie not found",
            };
        }
        const wishlistIsExist = await Wishlist.findOne({
            where: {
                movieId: id,
                userId: req.user.userId,
            },
        });
        if (wishlistIsExist) {
            throw {
                status: 400,
                message: "Movie is already exist on wishlist",
            };
        }
        const wishlist = await Wishlist.create({
            userId: req.user.userId,
            movieId: movie.id,
            title: movie.title,
            genre: movie.genre,
            creator: movie.creator,
            createdAt: new Date(),
            updatedAt: new Date(),
        });
        res.status(200).json({ wishlist });
    } catch (err) {
        next(err);
    }
};

exports.getWishlists = async(req, res, next) => {
    try {
        const wishlists = await Wishlist.findAll({
            where: {
                userId: req.user.userId,
            },
        });
        res.status(200).json({ wishlists });
    } catch (err) {
        next(err);
    }
};

exports.deleteWishlist = async(req, res, next) => {
    try {
        const { id } = req.params;
        const movie = await Movie.findByPk(id);
        if (!movie) {
            throw {
                status: 404,
                message: "Movie not found",
            };
        }
        const wishlist = await Wishlist.findOne({
            where: {
                movieId: id,
                userId: req.user.userId,
            },
        });
        if (!wishlist) {
            throw {
                status: 404,
                message: "Movie wishlists not found",
            };
        }
        await wishlist.destroy();
        res.status(200).json({ wishlist });
    } catch (err) {
        next(err);
    }
};
require("dotenv").config();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { User, Admin } = require("../../models");
const { validationResult } = require("express-validator");
const { OAuth2Client } = require("google-auth-library");
const client = new OAuth2Client({
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
});
const axios = require("axios");
const cloudinary = require("../../config/cloudinary");
const fs = require("fs");
const sendMail = require("../../config/sendMail");
const otp = require("otp-generator");

exports.postRegister = async(req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw {
                status: 400,
                message: errors,
            };
        }
        const { email, password, name, birthdate } = req.body;
        const photo_profile = await cloudinary.uploader.upload(req.file.path);
        fs.unlinkSync(req.file.path);
        let user = await User.findOne({
            where: {
                email: email,
            },
        });
        const admin = await Admin.findOne({
            where: {
                email: email,
            },
        });
        if (user || admin) {
            throw {
                status: 400,
                message: "Email is already exist",
            };
        }
        const hashedPw = await bcrypt.hash(password, 12);
        user = await User.create({
            email: email,
            password: hashedPw,
            name: name,
            birthdate: birthdate,
            createdAt: new Date(),
            updatedAt: new Date(),
            photo_profile: photo_profile.secure_url,
            photo_profile_id: photo_profile.public_id,
        });
        await sendMail(
            "hacktigo@gmail.com",
            user.email,
            "Created Account",
            `Your account ${user.email} has been created successfully`
        );
        res.status(201).json(user);
    } catch (err) {
        next(err);
    }
};

exports.postLogin = async(req, res, next) => {
    try {
        const { email, password, google_token, facebook_token } = req.body;
        if (email && password) {
            const user = await User.findOne({
                where: {
                    email: email,
                },
            });
            if (!user) {
                throw {
                    status: 404,
                    message: "User not found",
                };
            }
            const isPasswordMatched = await bcrypt.compare(password, user.password);
            if (!isPasswordMatched) {
                throw {
                    status: 401,
                    message: "Email / Password is wrong",
                };
            }
            const token = jwt.sign({
                    userId: user.id,
                    email: user.email,
                },
                process.env.JWT_SECRET
            );
            return res.status(200).json({ token });
        } else if (google_token) {
            const payload = await client.verifyIdToken({
                idToken: google_token,
                audience: process.env.CLIENT_IDs,
            });
            const email = payload.getPayload().email;
            const user = await User.findOne({
                where: {
                    email: email,
                },
            });
            const admin = await Admin.findOne({
                where: {
                    email: email,
                },
            });
            if (admin) {
                throw {
                    status: 400,
                    message: "Invalid email",
                };
            }
            if (!user) {
                const createdUser = await User.create({
                    email: email,
                });
                await sendMail(
                    "hacktigo@gmail.com",
                    createdUser.email,
                    "Created Account",
                    `Your account ${user.email} has been created successfully`
                );
                const token = jwt.sign({
                        id: createdUser.id,
                        email: createdUser.email,
                    },
                    process.env.JWT_SECRET
                );
                return res.status(200).json({ token });
            }
            const token = jwt.sign({
                    id: user.id,
                    email: user.email,
                },
                process.env.JWT_SECRET
            );
            return res.status(200).json({ token });
        } else if (facebook_token) {
            const URL_API_FB =
                "https://graph.facebook.com/v12.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday&access_token=";
            const response = await axios.get(`${URL_API_FB}${facebook_token}`);
            const email = response.data.email;
            const user = User.findOne({
                where: {
                    email: email,
                },
            });
            const admin = await Admin.findOne({
                where: {
                    email: email,
                },
            });
            if (admin) {
                throw {
                    status: 400,
                    message: "Invalid email",
                };
            }
            if (!user) {
                const createdUser = await User.create({
                    email: email,
                });
                await sendMail(
                    "hacktigo@gmail.com",
                    createdUser.email,
                    "Created Account",
                    `Your account ${user.email} has been created successfully`
                );
                const token = jwt.sign({
                        id: createdUser.id,
                        email: createdUser.email,
                    },
                    process.env.JWT_SECRET
                );
                return res.status(200).json({ token });
            }
            const token = jwt.sign({
                    id: user.id,
                    email: user.email,
                },
                process.env.JWT_SECRET
            );
            return res.status(200).json({ token });
        } else {
            throw {
                status: 400,
                message: "Bad Request",
            };
        }
    } catch (err) {
        next(err);
    }
};

exports.postForgotPasswordToken = async(req, res, next) => {
    try {
        const { email } = req.body;
        const user = await User.findOne({
            where: {
                email: email,
            },
        });
        if (!user) {
            throw {
                status: 404,
                message: "User not found",
            };
        }
        const otpGenerate = otp.generate(6, {
            upperCaseAlphabets: false,
            specialChars: false,
        });
        const hashOTP = await bcrypt.hash(otpGenerate, 12);
        await User.update({
            forgot_password_token: hashOTP,
            forgot_password_token_exp: new Date(new Date().getTime() + 3600000),
        }, {
            where: {
                email: email,
            },
        });
        const html = `
        <h2>Token anda : ${otpGenerate}</h2>
        <p>Token ini hanya berlaku 1 jam</p>
        `;
        await sendMail("hacktigo@gmail.com", email, "Forgot Password", null, html);
        res.status(200).json({
            message: "Email sent successfully",
        });
    } catch (err) {
        next(err);
    }
};

exports.postVerifyForgotPassword = async(req, res, next) => {
    try {
        const { email, token } = req.body;
        const user = await User.findOne({
            where: {
                email: email,
            },
        });
        if (!user) {
            throw {
                status: 404,
                message: "User not found",
            };
        }
        const isTokenValid = await bcrypt.compare(
            token,
            user.forgot_password_token
        );
        if (!isTokenValid) {
            throw {
                status: 400,
                message: "Token is invalid",
            };
        }
        if (new Date(user.forgot_password_token_exp) < new Date()) {
            throw {
                status: 400,
                message: "Token is expired",
            };
        }
        res.status(200).json({
            message: "TOKEN VALID",
        });
    } catch (err) {
        next(err);
    }
};

exports.postChangePassword = async(req, res, next) => {
    try {
        const { email, password, confPassword, token } = req.body;
        const user = await User.findOne({
            where: {
                email: email,
            },
        });
        if (!user) {
            throw {
                status: 404,
                message: "User not found",
            };
        }
        if (password !== confPassword) {
            throw {
                status: 400,
                message: "Password not match",
            };
        }
        const isTokenValid = await bcrypt.compare(
            token,
            user.forgot_password_token
        );
        if (!isTokenValid) {
            throw {
                status: 400,
                message: "Token is invalid",
            };
        }
        if (new Date(user.forgot_password_token_exp) < new Date()) {
            throw {
                status: 400,
                message: "Token is expired",
            };
        }
        const hashPassword = await bcrypt.hash(password, 12);
        await User.update({
            password: hashPassword,
            forgot_password_token: null,
            forgot_password_token_exp: null,
        }, {
            where: {
                email: email,
            },
        });
        res.status(200).json({
            message: "Password changed successfully",
        });
    } catch (err) {
        next(err);
    }
};
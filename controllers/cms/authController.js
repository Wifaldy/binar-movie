require("dotenv").config();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { Admin, User } = require("../../models");
const { validationResult } = require("express-validator");

exports.postRegister = async(req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw {
                status: 400,
                message: errors,
            };
        }
        const loggedAdmin = await Admin.findOne({
            where: {
                id: req.user.userId,
                role: "superadmin",
            },
        });
        if (!loggedAdmin) {
            throw {
                status: 401,
                message: "Unauthorized request",
            };
        }
        const { email, password, name, birthdate, role } = req.body;
        const user = await User.findOne({
            where: {
                email: email,
            },
        });
        let createdAdmin = await Admin.findOne({
            where: {
                email: email,
            },
        });
        if (user || createdAdmin) {
            throw {
                status: 400,
                message: "email is already exist",
            };
        }
        const hashedPw = await bcrypt.hash(password, 12);
        createdAdmin = await Admin.create({
            email: email,
            password: hashedPw,
            name: name,
            birthdate: birthdate,
            role: role,
            createdAt: new Date(),
            updatedAt: new Date(),
        });
        res.status(201).json(createdAdmin);
    } catch (err) {
        next(err);
    }
};

exports.postLogin = async(req, res, next) => {
    try {
        const { email, password } = req.body;
        const admin = await Admin.findOne({
            where: {
                email: email,
            },
        });
        if (!admin) {
            throw {
                status: 404,
                message: "User not found",
            };
        }
        const isPasswordMatched = await bcrypt.compare(password, admin.password);
        if (!isPasswordMatched) {
            throw {
                status: 401,
                message: "email / Password is wrong",
            };
        }
        const token = jwt.sign({
                userId: admin.id,
                email: admin.email,
            },
            process.env.JWT_SECRET
        );
        res.status(200).json({ token });
    } catch (err) {
        next(err);
    }
};
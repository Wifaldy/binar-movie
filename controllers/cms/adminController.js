const { Movie, User } = require("../../models");
const cloudinary = require("../../config/cloudinary");
const fs = require("fs");
const { Op } = require("sequelize");
const sendMail = require("../../config/sendMail");

exports.postCreateMovie = async(req, res, next) => {
    try {
        const { title, genre, creator } = req.body;
        const poster = await cloudinary.uploader.upload(req.file.path);
        fs.unlinkSync(req.file.path);
        const movie = await Movie.create({
            title: title,
            genre: genre,
            creator: creator,
            updatedAt: new Date(),
            createdAt: new Date(),
            poster: poster.secure_url,
            poster_id: poster.public_id,
        });
        res.status(200).json({ message: "Movie created successfully" });
    } catch (err) {
        next(err);
    }
};

exports.putUpdateMovie = async(req, res, next) => {
    try {
        const { title, genre, creator } = req.body;
        const { id } = req.params;
        let movie = await Movie.findByPk(id);
        if (!movie) {
            throw {
                status: 404,
                message: "Movie not found",
            };
        }
        await cloudinary.uploader.destroy(movie.poster_id);
        const poster = await cloudinary.uploader.upload(req.file.path);
        fs.unlinkSync(req.file.path);
        movie = await Movie.update({
            title: title,
            genre: genre,
            creator: creator,
            poster: poster.secure_url,
            poster_id: poster.public_id,
        }, {
            where: {
                id: id,
            },
            returning: true,
        });
        res.status(200).json(movie[1][0]);
    } catch (err) {
        next(err);
    }
};

exports.deleteMovie = async(req, res, next) => {
    try {
        const { id } = req.params;
        const movie = await Movie.findByPk(id);
        if (!movie) {
            throw {
                status: 404,
                message: "Movie not found",
            };
        }
        await movie.destroy();
        res.status(200).json(movie);
    } catch (err) {
        next(err);
    }
};

exports.postBroadcastEmail = async(req, res, next) => {
    try {
        const { userIds, subject, text } = req.body;
        const users = await User.findAll({
            where: {
                id: {
                    [Op.in]: userIds,
                },
            },
        });
        const emails = users.map((user) => user.email);
        emails.forEach(async(email) => {
            await sendMail("hacktigo@gmail.com", email, subject, text);
        });
        res.status(200).json({ message: "Email sent successfully" });
    } catch (err) {
        next(err);
    }
};
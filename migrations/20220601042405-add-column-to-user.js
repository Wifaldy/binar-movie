"use strict";

module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add altering commands here.
         *
         * Example:
         * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        await queryInterface.addColumn(
            "Users",
            "forgot_password_token",
            Sequelize.STRING
        );
        await queryInterface.addColumn(
            "Users",
            "forgot_password_token_exp",
            Sequelize.DATE
        );
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
        await queryInterface.removeColumn("Users", "forgot_password_token");
        await queryInterface.removeColumn("Users", "forgot_password_token_exp");
    },
};
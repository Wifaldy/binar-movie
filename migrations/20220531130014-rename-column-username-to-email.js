"use strict";

module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add altering commands here.
         *
         * Example:
         * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        await queryInterface.renameColumn("Users", "username", "email");
        await queryInterface.renameColumn("Admins", "username", "email");
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
        await queryInterface.renameColumn("Users", "email", "username");
        await queryInterface.renameColumn("Admins", "email", "username");
    },
};
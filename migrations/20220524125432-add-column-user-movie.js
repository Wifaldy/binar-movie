"use strict";

module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add altering commands here.
         *
         * Example:
         * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
         */
        await queryInterface.addColumn("Users", "photo_profile", Sequelize.STRING);
        await queryInterface.addColumn(
            "Users",
            "photo_profile_id",
            Sequelize.STRING
        );
        await queryInterface.addColumn("Movies", "poster", Sequelize.STRING);
        await queryInterface.addColumn("Movies", "poster_id", Sequelize.STRING);
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add reverting commands here.
         *
         * Example:
         * await queryInterface.dropTable('users');
         */
        await queryInterface.removeColumn("Users", "photo_profile");
        await queryInterface.removeColumn("Users", "photo_profile_id");
        await queryInterface.removeColumn("Movies", "poster");
        await queryInterface.removeColumn("Movies", "poster_id");
    },
};
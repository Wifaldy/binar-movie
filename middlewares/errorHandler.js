module.exports = (err, req, res, next) => {
    req.sentry.captureException(err);
    const status = err.status || 500;
    const message = err.message || "Internal server error";
    res.status(status).json({ message });
};
const jwt = require("jsonwebtoken");
require("dotenv").config();
const { User } = require("../models");

module.exports = async(req, res, next) => {
    try {
        const authHeader = req.header("Authorization");
        if (!authHeader) {
            throw {
                status: 401,
                message: "Authroization denied",
            };
        }
        const token = authHeader;
        let decoded;
        decoded = jwt.verify(token, process.env.JWT_SECRET);
        if (!decoded) {
            throw {
                status: 401,
                message: "Invalid Token",
            };
        }
        const checkUser = await User.findOne({
            where: {
                id: decoded.userId,
                email: decoded.email,
            },
        });
        if (!checkUser) {
            throw {
                status: 401,
                message: "Unauthorized request",
            };
        }
        req.user = decoded;
        next();
    } catch (err) {
        next(err);
    }
};
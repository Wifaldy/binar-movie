"use strict";
const bcrypt = require("bcryptjs");

module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        const hashedPw = await bcrypt.hash("123", 12);
        await queryInterface.bulkInsert("Users", [{
            email: "demo@gmail.com",
            password: hashedPw,
            name: "demo",
            birthdate: "2020-05-18",
            createdAt: new Date(),
            updatedAt: new Date(),
        }, ]);
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete("Users", null, {
            truncate: true,
            restartIdentity: true,
        });
    },
};
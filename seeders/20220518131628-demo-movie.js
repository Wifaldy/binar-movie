"use strict";

module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        await queryInterface.bulkInsert("Movies", [{
                title: "The Shawshank Redemption",
                genre: "Drama",
                creator: "Frank Darabont",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                title: "The Godfather",
                genre: "Drama",
                creator: "Francis Ford Coppola",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ]);
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete("Movies", null, {
            truncate: true,
            restartIdentity: true,
        });
    },
};
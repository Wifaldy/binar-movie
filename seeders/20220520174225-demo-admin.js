"use strict";
const bcrypt = require("bcryptjs");

module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        const hashedPw = await bcrypt.hash("123", 12);
        await queryInterface.bulkInsert("Admins", [{
                email: "admin@gmail.com",
                password: hashedPw,
                name: "admin",
                birthdate: "2020-05-18",
                role: "admin",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
            {
                email: "superadmin@gmail.com",
                password: hashedPw,
                name: "superadmin",
                birthdate: "2020-04-23",
                role: "superadmin",
                createdAt: new Date(),
                updatedAt: new Date(),
            },
        ]);
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete("Admins", null, {
            truncate: true,
            restartIdentity: true,
        });
    },
};
"use strict";

module.exports = {
    async up(queryInterface, Sequelize) {
        /**
         * Add seed commands here.
         *
         * Example:
         * await queryInterface.bulkInsert('People', [{
         *   name: 'John Doe',
         *   isBetaMember: false
         * }], {});
         */
        await queryInterface.bulkInsert("Wishlists", [{
            userId: 1,
            movieId: 1,
            title: "The Shawshank Redemption",
            genre: "Drama",
            creator: "Frank Darabont",
            createdAt: new Date(),
            updatedAt: new Date(),
        }, ]);
    },

    async down(queryInterface, Sequelize) {
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
        await queryInterface.bulkDelete("Wishlists", null, {
            truncate: true,
            restartIdentity: true,
        });
    },
};
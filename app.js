const express = require("express");
const app = express();
const userAuthRoute = require("./routes/user/auth");
const adminAuthRoute = require("./routes/cms/auth");
const movieRoute = require("./routes/movie");
const wishlistRoute = require("./routes/user/wishlist");
const adminRoute = require("./routes/cms/admin");

const errorHandler = require("./middlewares/errorHandler");
const sentry = require("@sentry/node");
const morgan = require("morgan");
require("dotenv").config();

sentry.init({
    dsn: process.env.SENTRY_DSN,
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use((req, res, next) => {
    req.sentry = sentry;
    next();
});

app.use(morgan("tiny"));
app.use("/user", wishlistRoute);
app.use("/user", userAuthRoute);
app.use("/admin", adminAuthRoute);
app.use("/admin", adminRoute);
app.use(movieRoute);

app.use(errorHandler);

app.listen(3000, () => {
    console.log("Server is running on port 3000");
});
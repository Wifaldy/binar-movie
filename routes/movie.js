const router = require("express").Router();
const movieController = require("../controllers/movieController");

router.get("/movies", movieController.getMovies);

module.exports = router;
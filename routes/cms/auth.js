const router = require("express").Router();
const authController = require("../../controllers/cms/authController");
const isAuth = require("../../middlewares/userIsAuth");
const { body } = require("express-validator");
const adminIsAuth = require("../../middlewares/adminIsAuth");

router.post(
    "/register", [
        body("email").isEmail().notEmpty().withMessage("Username is required"),
        body("password").notEmpty().withMessage("Password is required"),
        body("name").notEmpty().withMessage("Name is required"),
        body("birthdate").notEmpty().withMessage("Birth date is required"),
    ],
    adminIsAuth,
    authController.postRegister
);

router.post("/login", authController.postLogin);

module.exports = router;
const router = require("express").Router();
const adminController = require("../../controllers/cms/adminController");
const isAuth = require("../../middlewares/adminIsAuth");
const multer = require("multer");
const storage = require("../../config/multerStorage");
const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (
            file.mimetype === "image/jpeg" ||
            file.mimetype === "image/jpg" ||
            file.mimetype === "image/png"
        ) {
            cb(null, true);
        } else {
            cb(new Error("File should be an image"), false);
        }
    },
    limits: {
        fieldSize: 3000000,
    },
});

router.post(
    "/create-movie",
    isAuth,
    upload.single("poster"),
    adminController.postCreateMovie
);

router.put(
    "/update-movie/:id",
    isAuth,
    upload.single("poster"),
    adminController.putUpdateMovie
);

router.delete("/delete-movie/:id", isAuth, adminController.deleteMovie);

router.post("/broadcast-email", isAuth, adminController.postBroadcastEmail);

module.exports = router;
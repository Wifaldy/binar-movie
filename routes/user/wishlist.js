const router = require("express").Router();
const wishlistController = require("../../controllers/user/wishlistsController");
const isAuth = require("../../middlewares/userIsAuth");

router.get("/wishlists", isAuth, wishlistController.getWishlists);

router.post("/wishlist/:id", isAuth, wishlistController.postAddWishlist);

router.delete("/wishlist/:id", isAuth, wishlistController.deleteWishlist);

module.exports = router;
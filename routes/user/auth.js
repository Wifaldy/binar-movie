const router = require("express").Router();
const authController = require("../../controllers/user/authController");
const { body } = require("express-validator");
const multer = require("multer");
const storage = require("../../config/multerStorage");
const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (
            file.mimetype === "image/jpeg" ||
            file.mimetype === "image/jpg" ||
            file.mimetype === "image/png"
        ) {
            cb(null, true);
        } else {
            cb(new Error("File should be an image"), false);
        }
    },
    limits: {
        fieldSize: 3000000,
    },
});

router.post(
    "/register",
    upload.single("photo_profile"), [
        body("email").isEmail().notEmpty().withMessage("Username is required"),
        body("password").notEmpty().withMessage("Password is required"),
        body("name").notEmpty().withMessage("Name is required"),
        body("birthdate").notEmpty().withMessage("Birth date is required"),
    ],
    authController.postRegister
);

router.post("/login", authController.postLogin);

router.post("/forgot-password", authController.postForgotPasswordToken);

router.post("/verify-forgot-password", authController.postVerifyForgotPassword);

router.post("/change-password", authController.postChangePassword);

module.exports = router;